defmodule Aicacia.Autosheets.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add(:id, :uuid, primary_key: true)

      add(:username, :string, null: false)
      add(:encrypted_password, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:users, [:username]))
  end
end
