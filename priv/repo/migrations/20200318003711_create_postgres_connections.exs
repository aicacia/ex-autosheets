defmodule Aicacia.Autosheets.Repo.Migrations.CreatePostgresConnections do
  use Ecto.Migration

  def change do
    create table(:postgres_connections) do
      add(:connection_id, references(:connections, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:uri, :string, null: false)
      add(:username, :string, null: false)
      add(:password, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:postgres_connections, [:connection_id]))
  end
end
