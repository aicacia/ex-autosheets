defmodule Aicacia.Autosheets.Repo.Migrations.CreatePipes do
  use Ecto.Migration

  def change do
    create table(:pipes) do
      add(:organization_id, references(:organizations, on_delete: :delete_all, on_update: :nothing),
        null: false
      )

      add(:name, :string, null: false)
      add(:schedule, :string, null: false)
      add(:query, :string, null: false)

      add(:source_id, references(:connections, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:source_options, :map, default: "{}", null: false)

      add(:target_id, references(:connections, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:target_options, :map, default: "{}", null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:pipes, [:organization_id]))
    create(index(:pipes, [:source_id]))
    create(index(:pipes, [:target_id]))
    create(unique_index(:pipes, [:name]))
  end
end
