defmodule Aicacia.Autosheets.Repo.Migrations.CreateGoogleSheetConnections do
  use Ecto.Migration

  def change do
    create table(:google_sheet_connections) do
      add(:connection_id, references(:connections, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:service_account, :map, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:google_sheet_connections, [:connection_id]))
  end
end
