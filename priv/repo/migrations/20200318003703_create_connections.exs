defmodule Aicacia.Autosheets.Repo.Migrations.CreateConnections do
  use Ecto.Migration

  def change do
    create table(:connections) do
      add(:organization_id, references(:organizations, on_delete: :delete_all, on_update: :nothing),
        null: false
      )

      add(:name, :string, null: false)
      add(:type, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:connections, [:organization_id]))
    create(unique_index(:connections, [:name]))
  end
end
