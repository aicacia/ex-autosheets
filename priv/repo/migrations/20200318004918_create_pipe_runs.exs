defmodule Aicacia.Autosheets.Repo.Migrations.CreatePipeRuns do
  use Ecto.Migration

  def change do
    create table(:pipe_runs) do
      add(:pipe_id, references(:pipes, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:status, :string, null: false, default: "running")

      timestamps(type: :utc_datetime)
    end

    create(index(:pipe_runs, [:pipe_id]))
  end
end
