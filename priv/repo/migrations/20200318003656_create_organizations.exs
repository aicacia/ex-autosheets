defmodule Aicacia.Autosheets.Repo.Migrations.CreateOrganizations do
  use Ecto.Migration

  def change do
    create table(:organizations) do
      add(:owner_id, references(:users, type: :uuid, on_delete: :delete_all, on_update: :nothing),
        null: false
      )

      add(:name, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:organizations, [:name]))
  end
end
