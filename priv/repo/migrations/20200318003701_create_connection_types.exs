defmodule Aicacia.Autosheets.Repo.Migrations.CreateConnectionTypes do
  use Ecto.Migration

  def change do
    Aicacia.Autosheets.Model.ConnectionType.create_type()
  end
end
