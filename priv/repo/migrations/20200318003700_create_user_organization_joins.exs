defmodule Aicacia.Autosheets.Repo.Migrations.CreateUserOrganizationJoins do
  use Ecto.Migration

  def change do
    create table(:user_organization_joins, primary_key: false) do
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
      add(:organization_id, references(:organizations, on_delete: :delete_all, on_update: :nothing),
        null: false
      )
    end

    create(index(:user_organization_joins, [:user_id]))
    create(index(:user_organization_joins, [:organization_id]))
    create(unique_index(:user_organization_joins, [:user_id, :organization_id]))
  end
end
