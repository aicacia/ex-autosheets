defmodule Aicacia.Autosheets.Web.Schema.Pipe do
  alias OpenApiSpex.{Schema, Reference}

  defmodule Show do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Pipe.Show",
      description: "A pipe",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Pipe id"},
        organization: %Reference{"$ref": "#/components/schemas/Organization.PublicShow"},
        name: %Schema{type: :string, description: "Pipe name"},
        query: %Schema{type: :string, description: "Pipe query string"},
        schedule: %Schema{type: :string, description: "Pipe schedule"},
        source_id: %Schema{type: :integer, description: "Pipe Source Connection id"},
        source: %Reference{"$ref": "#/components/schemas/Connection.Show"},
        source_options: %Schema{type: :object, description: "Pipe Source Connection options"},
        target_id: %Schema{type: :integer, description: "Pipe Target Connection id"},
        target: %Reference{"$ref": "#/components/schemas/Connection.Show"},
        target_options: %Schema{type: :object, description: "Pipe Target Connection options"},
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :organization, :name, :query, :schedule, :source_id, :target_id],
      example: %{
        "id" => 1,
        "organization" => %{
          "id" => 1,
          "name" => "Organization",
          "owner" => %{
            "id" => "123e4567-e89b-12d3-a456-426614174000",
            "username" => "user_username",
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          },
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "name" => "Pipe",
        "query" => "SELECT * FROM table;",
        "schedule" => "0 0 * * * *",
        "source_id" => 1,
        "source" => %{
          "id" => 1,
          "organization" => %{
            "id" => 1,
            "name" => "Organization",
            "owner" => %{
              "id" => "123e4567-e89b-12d3-a456-426614174000",
              "username" => "user_username",
              "inserted_at" => "2017-09-12T12:34:55Z",
              "updated_at" => "2017-09-13T10:11:12Z"
            },
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          },
          "name" => "My Postgres Connection",
          "type" => "postgres",
          "uri" => "postgres://postgres-example.com:5432/database",
          "username" => "username",
          "password" => "password",
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "source_options" => %{},
        "target_id" => 2,
        "target" => %{
          "id" => 1,
          "organization" => %{
            "id" => 1,
            "name" => "Organization",
            "owner" => %{
              "id" => "123e4567-e89b-12d3-a456-426614174000",
              "username" => "user_username",
              "inserted_at" => "2017-09-12T12:34:55Z",
              "updated_at" => "2017-09-13T10:11:12Z"
            },
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          },
          "name" => "My Postgres Connection",
          "type" => "postgres",
          "uri" => "postgres://postgres-example.com:5432/database",
          "username" => "username",
          "password" => "password",
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "target_options" => %{},
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Index do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Pipe.Index",
      description: "All pipes",
      type: :array,
      items: Show,
      example: [
        OpenApiSpex.Schema.example(Show.schema())
      ]
    })
  end

  defmodule Create do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Pipe.Create",
      description: "A pipe create body",
      type: :object,
      properties: %{
        organization_id: %Schema{type: "string", description: "Organization id"},
        name: %Schema{type: :string, description: "Pipe name"},
        query: %Schema{type: :string, description: "Pipe query string"},
        schedule: %Schema{type: :string, description: "Pipe schedule"},
        source_id: %Schema{type: :integer, description: "Pipe Source Connection id"},
        source_options: %Schema{type: :object, description: "Pipe Source Connection options"},
        target_id: %Schema{type: :integer, description: "Pipe Target Connection id"},
        target_options: %Schema{type: :object, description: "Pipe Target Connection options"}
      },
      required: [:name, :query, :schedule, :source_id, :target_id],
      example: %{
        "organization_id" => 1,
        "name" => "Pipe",
        "query" => "SELECT * FROM table;",
        "schedule" => "0 0 * * * *",
        "source_id" => 1,
        "source_options" => %{},
        "target_id" => 2,
        "target_options" => %{}
      }
    })
  end

  defmodule Update do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Pipe.Update",
      description: "A pipe update body",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "Pipe name"},
        query: %Schema{type: :string, description: "Pipe query string"},
        schedule: %Schema{type: :string, description: "Pipe schedule"},
        source_id: %Schema{type: :integer, description: "Pipe Source Connection id"},
        source_options: %Schema{type: :object, description: "Pipe Source Connection options"},
        target_id: %Schema{type: :integer, description: "Pipe Target Connection id"},
        target_options: %Schema{type: :object, description: "Pipe Target Connection options"}
      },
      required: [],
      example: %{
        "name" => "Pipe",
        "query" => "SELECT * FROM table;",
        "schedule" => "0 0 * * * *",
        "source_id" => 1,
        "source_options" => %{},
        "target_id" => 2,
        "target_options" => %{}
      }
    })
  end
end
