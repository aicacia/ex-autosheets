defmodule Aicacia.Autosheets.Web.Schema.User do
  alias OpenApiSpex.Schema

  defmodule Public do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "User.Public",
      description: "A public user",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "User id"},
        username: %Schema{type: :string, description: "Username"},
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :inserted_at, :updated_at],
      example: %{
        "id" => "123e4567-e89b-12d3-a456-426614174000",
        "username" => "user_username",
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Private do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "User.Private",
      description: "A private user",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "User id"},
        username: %Schema{type: :string, description: "Username"},
        token: %Schema{type: :string, description: "User Token"},
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :username, :token, :inserted_at, :updated_at],
      example: %{
        "id" => "123e4567-e89b-12d3-a456-426614174000",
        "username" => "user_username",
        "token" => "a9psd8fhaowntw4iojha3084tjhap4jtq34tojapsjgaaaat5j955357f",
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end
end
