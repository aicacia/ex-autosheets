defmodule Aicacia.Autosheets.Web.Schema.Organization do
  alias OpenApiSpex.{Schema, Reference}

  defmodule Public do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Organization.Public",
      description: "All organizations",
      type: :array,
      items: %Schema{
        type: :object,
        properties: %{
          id: %Schema{type: :string, description: "Organization id"},
          name: %Schema{type: :string, description: "Organization name"},
          owner: %Reference{"$ref": "#/components/schemas/User.Public"},
          inserted_at: %Schema{
            type: :string,
            description: "Creation timestamp",
            format: :"date-time"
          },
          updated_at: %Schema{
            type: :string,
            description: "Updated at timestamp",
            format: :"date-time"
          }
        },
        required: [:id, :name, :owner]
      },
      example: %{
        "id" => 1,
        "name" => "Organization",
        "owner" => %{
          "id" => "123e4567-e89b-12d3-a456-426614174000",
          "username" => "user_username",
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Index do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Organization.Index",
      description: "All organizations",
      type: :array,
      items: Public,
      example: [
        OpenApiSpex.Schema.example(Public.schema())
      ]
    })
  end

  defmodule Private do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Organization.Private",
      description: "A organization",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Organization id"},
        name: %Schema{type: :string, description: "Organization name"},
        owner: %Schema{type: :object},
        users: %Schema{
          type: :array,
          items: %Schema{type: :object},
          description: "Organization users"
        },
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :name, :owner, :users],
      example: %{
        "id" => 1,
        "name" => "Organization",
        "owner" => %{
          "id" => "123e4567-e89b-12d3-a456-426614174000",
          "username" => "user_username",
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "users" => [
          %{
            "id" => "123e4567-e89b-12d3-a456-426614174000",
            "username" => "user_username",
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          }
        ],
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Create do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Organization.Create",
      description: "A organization create body",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "Organization name"}
      },
      required: [:name, :query, :schedule, :source_id, :target_id],
      example: %{
        "name" => "Organization"
      }
    })
  end

  defmodule Update do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Organization.Update",
      description: "A organization update body",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "Organization name"}
      },
      required: [],
      example: %{
        "name" => "Organization"
      }
    })
  end
end
