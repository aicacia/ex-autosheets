defmodule Aicacia.Autosheets.Web.Schema.Connection do
  alias OpenApiSpex.{Schema, Reference}

  defmodule ServiceAccount do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.ServiceAccount",
      description: "Service Account JSON",
      type: :object,
      properties: %{
        type: %Schema{type: :string},
        project_id: %Schema{type: :string},
        private_key_id: %Schema{type: :string},
        private_key: %Schema{type: :string},
        client_email: %Schema{type: :string, format: :email},
        client_id: %Schema{type: :string},
        auth_uri: %Schema{type: :string, format: :uri},
        token_uri: %Schema{type: :string, format: :uri},
        auth_provider_x509_cert_url: %Schema{type: :string, format: :uri},
        client_x509_cert_url: %Schema{type: :string, format: :uri}
      },
      required: [
        :type,
        :project_id,
        :private_key_id,
        :private_key,
        :client_email,
        :client_id,
        :auth_uri,
        :token_uri,
        :auth_provider_x509_cert_url,
        :client_x509_cert_url
      ],
      example: %{
        type: "service_account",
        project_id: "aicacia",
        private_key_id: "92854y90284j980asdf89ha8serh034",
        private_key: "--- private_key ---",
        client_email: "autosheets@aicacia.iam.gserviceaccount.com",
        client_id: "23847209587238945",
        auth_uri: "https://accounts.google.com/o/oauth2/auth",
        token_uri: "https://oauth2.googleapis.com/token",
        auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
        client_x509_cert_url:
          "https://www.googleapis.com/robot/v1/metadata/x509/autosheets%40aicacia.iam.gserviceaccount.com"
      }
    })
  end

  defmodule GoogleSheet do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.GoogleSheet",
      description: "A google sheet connection",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "id"},
        organization: %Reference{"$ref": "#/components/schemas/Organization.PublicShow"},
        name: %Schema{type: :string, description: "GoogleSheet Connection name"},
        type: %Schema{type: "google_sheet"},
        service_account: ServiceAccount,
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :name, :service_account, :inserted_at, :updated_at],
      example: %{
        "id" => 1,
        "organization" => %{
          "id" => 1,
          "name" => "Organization",
          "owner" => %{
            "id" => "123e4567-e89b-12d3-a456-426614174000",
            "username" => "user_username",
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          },
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "name" => "My Google Sheet Connection",
        "type" => "google_sheet",
        "service_account" => OpenApiSpex.Schema.example(ServiceAccount.schema()),
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Postgres do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.Postgres",
      description: "A postgres connection",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "id"},
        organization: %Reference{"$ref": "#/components/schemas/Organization.PublicShow"},
        name: %Schema{type: :string, description: "Postgres Connection name"},
        type: %Schema{type: "postgres"},
        uri: %Schema{type: :string, description: "Postgres URI"},
        username: %Schema{type: :string, description: "Postgres username"},
        password: %Schema{type: :string, description: "Postgres password"},
        inserted_at: %Schema{
          type: :string,
          description: "Creation timestamp",
          format: :"date-time"
        },
        updated_at: %Schema{
          type: :string,
          description: "Updated at timestamp",
          format: :"date-time"
        }
      },
      required: [:id, :name, :uri, :username, :password, :inserted_at, :updated_at],
      example: %{
        "id" => 1,
        "organization" => %{
          "id" => 1,
          "name" => "Organization",
          "owner" => %{
            "id" => "123e4567-e89b-12d3-a456-426614174000",
            "username" => "user_username",
            "inserted_at" => "2017-09-12T12:34:55Z",
            "updated_at" => "2017-09-13T10:11:12Z"
          },
          "inserted_at" => "2017-09-12T12:34:55Z",
          "updated_at" => "2017-09-13T10:11:12Z"
        },
        "name" => "My Postgres Connection",
        "type" => "postgres",
        "uri" => "postgres://postgres-example.com:5432/database",
        "username" => "username",
        "password" => "password",
        "inserted_at" => "2017-09-12T12:34:55Z",
        "updated_at" => "2017-09-13T10:11:12Z"
      }
    })
  end

  defmodule Show do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.Show",
      description: "A connection",
      type: :object,
      oneOf: [
        GoogleSheet,
        Postgres
      ],
      example: OpenApiSpex.Schema.example(Postgres.schema())
    })
  end

  defmodule Index do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.Index",
      description: "All connections",
      type: :array,
      items: Show,
      example: [
        OpenApiSpex.Schema.example(Postgres.schema()),
        OpenApiSpex.Schema.example(GoogleSheet.schema())
      ]
    })
  end

  defmodule GoogleSheetCreate do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.GoogleSheetCreate",
      description: "A google sheet connection create body",
      type: :object,
      properties: %{
        organization_id: %Schema{type: "string", description: "Organization id"},
        name: %Schema{type: :string, description: "GoogleSheet Connection name"},
        type: %Schema{type: "google_sheet"},
        service_account: ServiceAccount
      },
      required: [:name, :service_account],
      example: %{
        "organization_id" => 1,
        "name" => "My Google Sheet Connection",
        "service_account" => OpenApiSpex.Schema.example(ServiceAccount.schema())
      }
    })
  end

  defmodule PostgresCreate do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.PostgresCreate",
      description: "A postgres connection create body",
      type: :object,
      properties: %{
        organization_id: %Schema{type: "string", description: "Organization id"},
        name: %Schema{type: :string, description: "Postgres Connection name"},
        type: %Schema{type: "postgres"},
        uri: %Schema{type: :string, description: "Postgres URI"},
        username: %Schema{type: :string, description: "Postgres username"},
        password: %Schema{type: :string, description: "Postgres password"}
      },
      required: [:name, :uri, :username, :password],
      example: %{
        "organization_id" => 1,
        "name" => "My Postgres Connection",
        "uri" => "postgres://postgres-example.com:5432/database",
        "username" => "username",
        "password" => "password"
      }
    })
  end

  defmodule Create do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.Create",
      description: "Connection create body",
      type: :object,
      oneOf: [
        GoogleSheetCreate,
        PostgresCreate
      ],
      example: OpenApiSpex.Schema.example(PostgresCreate.schema())
    })
  end

  defmodule GoogleSheetUpdate do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.GoogleSheetUpdate",
      description: "A google sheet connection update body",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "GoogleSheet Connection name"},
        service_account: ServiceAccount
      },
      required: [],
      example: %{
        "name" => "My Google Sheet Connection",
        "service_account" => OpenApiSpex.Schema.example(ServiceAccount.schema())
      }
    })
  end

  defmodule PostgresUpdate do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.PostgresUpdate",
      description: "A postgres connection update body",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "Postgres Connection name"},
        uri: %Schema{type: :string, description: "Postgres URI"},
        username: %Schema{type: :string, description: "Postgres username"},
        password: %Schema{type: :string, description: "Postgres password"}
      },
      required: [],
      example: %{
        "name" => "My Postgres Connection",
        "uri" => "postgres://postgres-example.com:5432/database",
        "username" => "username",
        "password" => "password"
      }
    })
  end

  defmodule Update do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Connection.Update",
      description: "Connection update body",
      type: :object,
      oneOf: [
        GoogleSheetUpdate,
        PostgresUpdate
      ],
      example: OpenApiSpex.Schema.example(PostgresUpdate.schema())
    })
  end
end
