defmodule Aicacia.Autosheets.Web.Router do
  use Aicacia.Autosheets.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {Aicacia.Autosheets.Web.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :user_authenticated do
    plug(Aicacia.Autosheets.Web.Plug.UserAuthentication)
  end

  pipeline :api_spec do
    plug OpenApiSpex.Plug.PutApiSpec, module: Aicacia.Autosheets.Web.ApiSpec
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/dashboard" do
      pipe_through :browser

      live_dashboard "/",
        metrics: Aicacia.Autosheets.Web.Telemetry,
        ecto_repos: [Aicacia.Autosheets.Repo]
    end

    scope "/swagger" do
      pipe_through :browser

      get "/", OpenApiSpex.Plug.SwaggerUI,
        path: "/api/swagger.json",
        default_model_expand_depth: 10,
        display_operation_id: true
    end
  end

  scope "/api" do
    pipe_through :api
    pipe_through :api_spec
    get "/swagger.json", OpenApiSpex.Plug.RenderSpec, []
  end

  scope "/api", Aicacia.Autosheets.Web.Controller.Api, as: :api do
    pipe_through :api
    pipe_through :api_spec

    get "/health", HealthCheck, :health
    head "/health", HealthCheck, :health

    resources "/organizations", Organization,
      only: [:index, :show, :new, :create, :edit, :update, :delete] do
      resources "/connections", Connection, only: [:index, :show, :create, :update, :delete]
      resources "/pipes", Pipe, only: [:index, :show, :create, :update, :delete]
    end
  end

  scope "/", Aicacia.Autosheets.Web.Controller do
    pipe_through :browser

    resources "/organizations", Organization,
      only: [:index, :show, :new, :create, :edit, :update, :delete] do
      resources "/connections", Connection,
        only: [:index, :show, :new, :create, :edit, :update, :delete]

      resources "/pipes", Pipe, only: [:index, :show, :new, :create, :edit, :update, :delete]
    end
  end
end
