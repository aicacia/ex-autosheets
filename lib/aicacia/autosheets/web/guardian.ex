defmodule Aicacia.Autosheets.Web.Guardian do
  use Guardian, otp_app: :aicacia_autosheets

  def subject_for_token(%{id: id}, _claims) do
    {:ok, to_string(id)}
  end

  def subject_for_token(_, _) do
    {:error, :invalid_error}
  end

  def resource_from_claims(%{"sub" => id}) do
    Aicacia.Autosheets.Service.User.Show.handle(%{id: id})
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end
