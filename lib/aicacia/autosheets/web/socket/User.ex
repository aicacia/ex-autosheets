defmodule Aicacia.Autosheets.Web.Socket.User do
  use Phoenix.Socket

  def connect(%{"user_id" => user_id, "token" => token}, socket, _connect_info) do
    case Guardian.Phoenix.Socket.authenticate(socket, Aicacia.Autosheets.Web.Guardian, token) do
      {:ok, authed_socket} ->
        {:ok, assign(authed_socket, user_id: user_id, token: token)}

      {:error, _} ->
        :error
    end
  end

  def connect(_params, _socket, _connect_info), do: :error

  def id(socket), do: "#{socket.assigns.user_id}"
end
