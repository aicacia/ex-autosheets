defmodule Aicacia.Autosheets.Web.Controller.Connection do
  use Aicacia.Autosheets.Web, :controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View

  action_fallback Aicacia.Autosheets.Web.Controller.Fallback

  def index(conn, params) do
    with {:ok, command} <- Service.Connection.Index.new(params),
         {:ok, connections} <- Service.Connection.Index.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("index.html", connections: connections)
    end
  end

  def show(conn, params) do
    with {:ok, command} <- Service.Connection.Show.new(params),
         {:ok, connection} <- Service.Connection.Show.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("show.html", connection: connection)
    end
  end

  def new(conn, _params) do
    changeset = Service.Connection.Create.changeset(%{})

    conn
    |> put_view(View.Connection)
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"create" => params}) do
    with {:ok, command} <- Service.Connection.Create.new(params),
         {:ok, connection} <- Service.Connection.Create.handle(command) do
      conn
      |> put_flash(:info, "Connection created successfully.")
      |> redirect(to: Routes.organization_connection_path(conn, :show, connection))
    end
  end

  def edit(conn, params) do
    connection = Service.Connection.Show.new!(params) |> Service.Connection.Show.handle!()
    changeset = Service.Connection.Update.from(connection)

    conn
    |> put_view(View.Connection)
    |> render("edit.html", connection: connection, changeset: changeset)
  end

  def update(conn, %{"update" => params}) do
    with {:ok, command} <- Service.Connection.Update.new(params),
         {:ok, connection} <- Service.Connection.Update.handle(command) do
      conn
      |> put_flash(:info, "Connection updated successfully.")
      |> redirect(to: Routes.organization_connection_path(conn, :show, connection))
    end
  end

  def delete(conn, params) do
    with {:ok, command} <- Service.Connection.Delete.new(params),
         {:ok, _connection} <- Service.Connection.Delete.handle(command) do
      conn
      |> put_flash(:info, "Connection deleted successfully.")
      |> redirect(to: Routes.organization_connection_path(conn, :index))
    end
  end
end
