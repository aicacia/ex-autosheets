defmodule Aicacia.Autosheets.Web.Controller.Pipe do
  use Aicacia.Autosheets.Web, :controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View

  action_fallback Aicacia.Autosheets.Web.Controller.Fallback

  def index(conn, params) do
    with {:ok, command} <- Service.Pipe.Index.new(params),
         {:ok, pipes} <- Service.Pipe.Index.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("index.html", pipes: pipes)
    end
  end

  def show(conn, params) do
    with {:ok, command} <- Service.Pipe.Show.new(params),
         {:ok, pipe} <- Service.Pipe.Show.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("show.html", pipe: pipe)
    end
  end

  def new(conn, _params) do
    changeset = Service.Pipe.Create.changeset(%{})

    conn
    |> put_view(View.Pipe)
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"create" => params}) do
    with {:ok, command} <- Service.Pipe.Create.new(params),
         {:ok, pipe} <- Service.Pipe.Create.handle(command) do
      conn
      |> put_flash(:info, "Pipe created successfully.")
      |> redirect(to: Routes.organization_pipe_path(conn, :show, pipe))
    end
  end

  def edit(conn, params) do
    pipe = Service.Pipe.Show.new!(params) |> Service.Pipe.Show.handle!()
    changeset = Service.Pipe.Update.from(pipe)

    conn
    |> put_view(View.Pipe)
    |> render("edit.html", pipe: pipe, changeset: changeset)
  end

  def update(conn, %{"update" => params}) do
    with {:ok, command} <- Service.Pipe.Update.new(params),
         {:ok, pipe} <- Service.Pipe.Update.handle(command) do
      conn
      |> put_flash(:info, "Pipe updated successfully.")
      |> redirect(to: Routes.organization_pipe_path(conn, :show, pipe))
    end
  end

  def delete(conn, params) do
    with {:ok, command} <- Service.Pipe.Delete.new(params),
         {:ok, _pipe} <- Service.Pipe.Delete.handle(command) do
      conn
      |> put_flash(:info, "Pipe deleted successfully.")
      |> redirect(to: Routes.organization_pipe_path(conn, :index))
    end
  end
end
