defmodule Aicacia.Autosheets.Web.Controller.Api.Organization do
  @moduledoc tags: ["Organization"]

  use Aicacia.Autosheets.Web, :controller
  use OpenApiSpex.Controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View
  alias Aicacia.Autosheets.Web.Schema

  action_fallback Aicacia.Autosheets.Web.Controller.Api.Fallback

  @doc """
  Gets all organizations

  Returns all organizations
  """
  @doc parameters: [
         page: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page"
         ],
         size: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page Size"
         ]
       ],
       responses: [
         ok: {"All Organizations Response", "application/json", Schema.Organization.Index}
       ]
  def index(conn, params) do
    with {:ok, command} <- Service.Organization.Index.new(params),
         {:ok, organizations} <- Service.Organization.Index.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("index.json", organizations: organizations)
    end
  end

  @doc """
  Gets a organization

  Returns a organization
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Organization Response", "application/json", Schema.Organization.Public}
       ]
  def show(conn, params) do
    with {:ok, command} <- Service.Organization.Show.new(params),
         {:ok, organization} <- Service.Organization.Show.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("show.json", organization: organization)
    end
  end

  @doc """
  Creates a organization

  Returns created organization
  """
  @doc request_body:
         {"Request body to create a Organization", "application/json", Schema.Organization.Create,
          required: true},
       responses: [
         ok: {"Organization Response", "application/json", Schema.Organization.Public}
       ]
  def create(conn, params) do
    with {:ok, command} <- Service.Organization.Create.new(params),
         {:ok, organization} <- Service.Organization.Create.handle(command) do
      conn
      |> put_view(View.Organization)
      |> put_status(:created)
      |> render("show.json", organization: organization)
    end
  end

  @doc """
  Updates a organization

  Returns updated organization
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       request_body:
         {"Request body to create a Organization", "application/json", Schema.Organization.Update,
          required: true},
       responses: [
         ok: {"Organization Response", "application/json", Schema.Organization.Public}
       ]
  def update(conn, params) do
    with {:ok, command} <- Service.Organization.Update.new(params),
         {:ok, organization} <- Service.Organization.Update.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("show.json", organization: organization)
    end
  end

  @doc """
  Deletes a organization

  Returns deleted organization
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Organization Response", "application/json", Schema.Organization.Public}
       ]
  def delete(conn, params) do
    with {:ok, command} <- Service.Organization.Delete.new(params),
         {:ok, organization} <- Service.Organization.Delete.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("show.json", organization: organization)
    end
  end
end
