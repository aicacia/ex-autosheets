defmodule Aicacia.Autosheets.Web.Controller.Api.Fallback do
  use Aicacia.Autosheets.Web, :controller

  alias Aicacia.Autosheets.Web.View

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(View.Changeset)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, %Ecto.InvalidChangesetError{changeset: changeset}}) do
    call(conn, {:error, changeset})
  end

  def call(conn, {:error, %Ecto.NoResultsError{}}) do
    conn
    |> put_status(:not_found)
    |> put_view(View.Error)
    |> render(:"404")
  end

  if Mix.env() in [:dev, :test] do
    def call(conn, {:error, %{message: message}}) do
      conn
      |> put_status(500)
      |> put_view(View.Error)
      |> json(%{detail: message})
    end
  end
end
