defmodule Aicacia.Autosheets.Web.Controller.Api.Pipe do
  @moduledoc tags: ["Pipe"]

  use Aicacia.Autosheets.Web, :controller
  use OpenApiSpex.Controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View
  alias Aicacia.Autosheets.Web.Schema

  action_fallback Aicacia.Autosheets.Web.Controller.Api.Fallback

  @doc """
  Gets all pipes

  Returns all pipes
  """
  @doc parameters: [
         page: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page"
         ],
         size: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page Size"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"All Pipes Response", "application/json", Schema.Pipe.Index}
       ]
  def index(conn, params) do
    with {:ok, command} <- Service.Pipe.Index.new(params),
         {:ok, pipes} <- Service.Pipe.Index.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("index.json", pipes: pipes)
    end
  end

  @doc """
  Gets a pipe

  Returns a pipe
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Pipe id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Pipe Response", "application/json", Schema.Pipe.Show}
       ]
  def show(conn, params) do
    with {:ok, command} <- Service.Pipe.Show.new(params),
         {:ok, pipe} <- Service.Pipe.Show.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("show.json", pipe: pipe)
    end
  end

  @doc """
  Runs a pipe

  Returns a pipe
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Pipe id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Pipe Response", "application/json", Schema.Pipe.Show}
       ]
  def run(conn, params) do
    with {:ok, command} <- Service.Pipe.Run.new(params),
         {:ok, pipe} <- Service.Pipe.Run.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("show.json", pipe: pipe)
    end
  end

  @doc """
  Creates a pipe

  Returns created pipe
  """
  @doc parameters: [
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       request_body:
         {"Request body to create a Pipe", "application/json", Schema.Pipe.Create, required: true},
       responses: [
         ok: {"Pipe Response", "application/json", Schema.Pipe.Show}
       ]
  def create(conn, params) do
    with {:ok, command} <- Service.Pipe.Create.new(params),
         {:ok, pipe} <- Service.Pipe.Create.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> put_status(:created)
      |> render("show.json", pipe: pipe)
    end
  end

  @doc """
  Updates a pipe

  Returns updated pipe
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Pipe id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       request_body:
         {"Request body to create a Pipe", "application/json", Schema.Pipe.Update, required: true},
       responses: [
         ok: {"Pipe Response", "application/json", Schema.Pipe.Show}
       ]
  def update(conn, params) do
    with {:ok, command} <- Service.Pipe.Update.new(params),
         {:ok, pipe} <- Service.Pipe.Update.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("show.json", pipe: pipe)
    end
  end

  @doc """
  Deletes a pipe

  Returns deleted pipe
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Pipe id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Pipe Response", "application/json", Schema.Pipe.Show}
       ]
  def delete(conn, params) do
    with {:ok, command} <- Service.Pipe.Delete.new(params),
         {:ok, pipe} <- Service.Pipe.Delete.handle(command) do
      conn
      |> put_view(View.Pipe)
      |> render("show.json", pipe: pipe)
    end
  end
end
