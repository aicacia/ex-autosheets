defmodule Aicacia.Autosheets.Web.Controller.Api.Connection do
  @moduledoc tags: ["Connection"]

  use Aicacia.Autosheets.Web, :controller
  use OpenApiSpex.Controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View
  alias Aicacia.Autosheets.Web.Schema

  action_fallback Aicacia.Autosheets.Web.Controller.Api.Fallback

  @doc """
  Gets all connections

  Returns all connections
  """
  @doc parameters: [
         page: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page"
         ],
         size: [
           in: :query,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Page Size"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"All Connections Response", "application/json", Schema.Connection.Index}
       ]
  def index(conn, params) do
    with {:ok, command} <- Service.Connection.Index.new(params),
         {:ok, connections} <- Service.Connection.Index.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("index.json", connections: connections)
    end
  end

  @doc """
  Gets a connection

  Returns a connection
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Connection id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Connection Response", "application/json", Schema.Connection.Show}
       ]
  def show(conn, params) do
    with {:ok, command} <- Service.Connection.Show.new(params),
         {:ok, connection} <- Service.Connection.Show.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("show.json", connection: connection)
    end
  end

  @doc """
  Creates a connection

  Returns created connection
  """
  @doc parameters: [
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       request_body:
         {"Request body to create a Connection", "application/json", Schema.Connection.Create,
          required: true},
       responses: [
         ok: {"Connection Response", "application/json", Schema.Connection.Show}
       ]
  def create(conn, params) do
    with {:ok, command} <- Service.Connection.Create.new(params),
         {:ok, connection} <- Service.Connection.Create.handle(command) do
      conn
      |> put_view(View.Connection)
      |> put_status(:created)
      |> render("show.json", connection: connection)
    end
  end

  @doc """
  Updates a connection

  Returns updated connection
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Connection id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       request_body:
         {"Request body to create a Connection", "application/json", Schema.Connection.Update,
          required: true},
       responses: [
         ok: {"Connection Response", "application/json", Schema.Connection.Show}
       ]
  def update(conn, params) do
    with {:ok, command} <- Service.Connection.Update.new(params),
         {:ok, connection} <- Service.Connection.Update.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("show.json", connection: connection)
    end
  end

  @doc """
  Deletes a connection

  Returns deleted connection
  """
  @doc parameters: [
         id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Connection id"
         ],
         organization_id: [
           in: :path,
           schema: %OpenApiSpex.Schema{type: :integer},
           description: "Organization id"
         ]
       ],
       responses: [
         ok: {"Connection Response", "application/json", Schema.Connection.Show}
       ]
  def delete(conn, params) do
    with {:ok, command} <- Service.Connection.Delete.new(params),
         {:ok, connection} <- Service.Connection.Delete.handle(command) do
      conn
      |> put_view(View.Connection)
      |> render("show.json", connection: connection)
    end
  end
end
