defmodule Aicacia.Autosheets.Web.Controller.Organization do
  use Aicacia.Autosheets.Web, :controller

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Web.View

  action_fallback Aicacia.Autosheets.Web.Controller.Fallback

  def index(conn, params) do
    with {:ok, command} <- Service.Organization.Index.new(params),
         {:ok, organizations} <- Service.Organization.Index.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("index.html", organizations: organizations)
    end
  end

  def show(conn, params) do
    with {:ok, command} <- Service.Organization.Show.new(params),
         {:ok, organization} <- Service.Organization.Show.handle(command) do
      conn
      |> put_view(View.Organization)
      |> render("show.html", organization: organization)
    end
  end

  def new(conn, _params) do
    changeset = Service.Organization.Create.changeset(%{})

    conn
    |> put_view(View.Organization)
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"create" => params}) do
    with {:ok, command} <- Service.Organization.Create.new(params),
         {:ok, organization} <- Service.Organization.Create.handle(command) do
      conn
      |> put_flash(:info, "Organization created successfully.")
      |> redirect(to: Routes.organization_path(conn, :show, organization))
    end
  end

  def edit(conn, params) do
    organization = Service.Organization.Show.new!(params) |> Service.Organization.Show.handle!()
    changeset = Service.Organization.Update.from(organization)

    conn
    |> put_view(View.Organization)
    |> render("edit.html", organization: organization, changeset: changeset)
  end

  def update(conn, %{"update" => params}) do
    with {:ok, command} <- Service.Organization.Update.new(params),
         {:ok, organization} <- Service.Organization.Update.handle(command) do
      conn
      |> put_flash(:info, "Organization updated successfully.")
      |> redirect(to: Routes.organization_path(conn, :show, organization))
    end
  end

  def delete(conn, params) do
    with {:ok, command} <- Service.Organization.Delete.new(params),
         {:ok, _organization} <- Service.Organization.Delete.handle(command) do
      conn
      |> put_flash(:info, "Organization deleted successfully.")
      |> redirect(to: Routes.organization_path(conn, :index))
    end
  end
end
