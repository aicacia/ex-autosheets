defmodule Aicacia.Autosheets.Web.Live.Organization.Form do
  use Aicacia.Autosheets.Web, :live_view

  alias Aicacia.Autosheets.Service

  @impl true
  def mount(_params, session, socket) do
    {id, changeset} =
      case Map.get(session, "organization") do
        nil ->
          {nil, Service.Organization.Create.changeset(%{})}

        organization ->
          {organization.id, Service.Organization.Update.from(organization)}
      end

    {:ok, assign(socket, changeset: changeset, id: id)}
  end

  @impl true
  def handle_event("validate", %{"organization" => params}, socket) do
    changeset =
      case socket.assigns.id do
        nil ->
          Service.Organization.Create.changeset(params) |> Map.put(:action, :insert)

        id ->
          Service.Organization.Update.changeset(Map.put(params, "id", id))
          |> Map.put(:action, :update)
      end

    {:noreply, assign(socket, changeset: changeset)}
  end

  @impl true
  def handle_event("save", %{"organization" => params}, socket) do
    organization =
      case socket.assigns.action do
        nil ->
          Service.Organization.Create.new!(params) |> Service.Organization.Create.handle!()

        id ->
          Service.Organization.Update.new!(Map.put(params, "id", id))
          |> Service.Organization.Update.handle!()
      end

    {:noreply,
     socket
     |> put_flash(:info, "organization saved")
     |> redirect(to: Routes.organization_path(socket, :show, organization))}
  end
end
