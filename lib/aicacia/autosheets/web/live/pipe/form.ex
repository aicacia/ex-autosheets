defmodule Aicacia.Autosheets.Web.Live.Pipe.Form do
  use Aicacia.Autosheets.Web, :live_view

  alias Aicacia.Autosheets.Service

  @impl true
  def mount(_params, session, socket) do
    {id, changeset} =
      case Map.get(session, "pipe") do
        nil ->
          {nil, Service.Pipe.Create.changeset(%{})}

        pipe ->
          {pipe.id, Service.Pipe.Update.from(pipe)}
      end

    {:ok, assign(socket, changeset: changeset, id: id)}
  end

  @impl true
  def handle_event("validate", %{"pipe" => params}, socket) do
    changeset =
      case socket.assigns.id do
        nil ->
          Service.Pipe.Create.changeset(params) |> Map.put(:action, :insert)

        id ->
          Service.Pipe.Update.changeset(Map.put(params, "id", id))
          |> Map.put(:action, :update)
      end

    {:noreply, assign(socket, changeset: changeset)}
  end

  @impl true
  def handle_event("save", %{"pipe" => params}, socket) do
    pipe =
      case socket.assigns.action do
        nil ->
          Service.Pipe.Create.new!(params) |> Service.Pipe.Create.handle!()

        id ->
          Service.Pipe.Update.new!(Map.put(params, "id", id)) |> Service.Pipe.Update.handle!()
      end

    {:noreply,
     socket
     |> put_flash(:info, "pipe saved")
     |> redirect(to: Routes.organization_pipe_path(socket, :show, pipe))}
  end
end
