defmodule Aicacia.Autosheets.Web.Live.Connection.Form do
  use Aicacia.Autosheets.Web, :live_view

  alias Aicacia.Autosheets.Service

  @impl true
  def mount(_params, session, socket) do
    {id, changeset} =
      case Map.get(session, "connection") do
        nil ->
          {nil, Service.Connection.Create.changeset(%{})}

        connection ->
          {connection.connection.id, Service.Connection.Update.from(connection)}
      end

    {:ok, assign(socket, changeset: changeset, id: id)}
  end

  @impl true
  def handle_event("validate", %{"connection" => params}, socket) do
    changeset =
      case socket.assigns.id do
        nil ->
          Service.Connection.Create.changeset(params) |> Map.put(:action, :insert)

        id ->
          Service.Connection.Update.changeset(Map.put(params, "id", id))
          |> Map.put(:action, :update)
      end

    {:noreply, assign(socket, changeset: changeset)}
  end

  @impl true
  def handle_event("save", %{"connection" => params}, socket) do
    connection =
      case socket.assigns.id do
        nil ->
          Service.Connection.Create.new!(params) |> Service.Connection.Create.handle!()

        id ->
          Service.Connection.Update.new!(Map.put(params, "id", id))
          |> Service.Connection.Update.handle!()
      end

    {:noreply,
     socket
     |> put_flash(:info, "connection saved")
     |> redirect(to: Routes.organization_connection_path(socket, :show, connection))}
  end
end
