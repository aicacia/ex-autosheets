defmodule Aicacia.Autosheets.Web.View.Organization do
  use Aicacia.Autosheets.Web, :view

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Web.View

  def render("index.json", %{organizations: organizations}) do
    render_many(organizations, View.Organization, "organization.json")
  end

  def render("show.json", %{organization: organization}) do
    render_one(organization, View.Organization, "organization.json")
  end

  def render("organization.json", %{
        organization: %Model.Organization{
          id: id,
          name: name,
          owner: owner,
          inserted_at: inserted_at,
          updated_at: updated_at
        }
      }) do
    %{
      id: id,
      name: name,
      owner: View.User.render_public_user(owner),
      inserted_at: inserted_at,
      updated_at: updated_at
    }
  end
end
