defmodule Aicacia.Autosheets.Web.View.Pipe do
  use Aicacia.Autosheets.Web, :view

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Web.View

  def render("index.json", %{pipes: pipes}) do
    render_many(pipes, View.Pipe, "pipe.json")
  end

  def render("show.json", %{pipe: pipe}) do
    render_one(pipe, View.Pipe, "pipe.json")
  end

  def render("pipe.json", %{
        pipe: %Model.Pipe{
          id: id,
          name: name,
          query: query,
          schedule: schedule,
          target_id: target_id,
          target: target,
          target_options: target_options,
          source_id: source_id,
          source: source,
          source_options: source_options,
          inserted_at: inserted_at,
          updated_at: updated_at
        }
      }) do
    %{
      id: id,
      name: name,
      query: query,
      schedule: schedule,
      target_id: target_id,
      target: View.Connection.render_connection(target),
      target_options: target_options,
      source_id: source_id,
      source: View.Connection.render_connection(source),
      source_options: source_options,
      inserted_at: inserted_at,
      updated_at: updated_at
    }
  end
end
