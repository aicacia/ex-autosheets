defmodule Aicacia.Autosheets.Web.View.User do
  use Aicacia.Autosheets.Web, :view

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Web.View

  def render("index.json", %{users: users}) do
    render_many(users, View.User, "public_user.json")
  end

  def render("show.json", %{user: user}) do
    render_one(user, View.User, "private_user.json")
  end

  def render("public_user.json", %{
        user: user
      }),
      do: render_public_user(user)

  def render("private_user.json", %{
        user: user
      }),
      do: render_private_user(user)

  def render_public_user(%Model.User{
        id: id,
        username: username,
        inserted_at: inserted_at,
        updated_at: updated_at
      }),
      do: %{
        id: id,
        username: username,
        inserted_at: inserted_at,
        updated_at: updated_at
      }

  def render_private_user(%Model.User{
        id: id,
        owned_organizations: owned_organizations,
        organizations: organizations,
        username: username,
        inserted_at: inserted_at,
        updated_at: updated_at
      }),
      do: %{
        id: id,
        username: username,
        owned_organizations: owned_organizations,
        organizations: organizations,
        inserted_at: inserted_at,
        updated_at: updated_at
      }
end
