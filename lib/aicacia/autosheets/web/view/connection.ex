defmodule Aicacia.Autosheets.Web.View.Connection do
  use Aicacia.Autosheets.Web, :view

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Web.View

  def render("index.json", %{connections: connections}) do
    render_many(connections, View.Connection, "connection.json")
  end

  def render("show.json", %{connection: connection}) do
    render_one(connection, View.Connection, "connection.json")
  end

  def render("connection.json", %{
        connection: connection
      }),
      do: render_connection(connection)

  def render_connection(%Model.GoogleSheetConnection{
        id: id,
        service_account: service_account,
        connection: %Model.Connection{name: name, type: type},
        inserted_at: inserted_at,
        updated_at: updated_at
      }),
      do: %{
        id: id,
        name: name,
        type: type,
        service_account: service_account,
        inserted_at: inserted_at,
        updated_at: updated_at
      }

  def render_connection(%Model.PostgresConnection{
        id: id,
        uri: uri,
        username: username,
        password: password,
        connection: %Model.Connection{name: name, type: type},
        inserted_at: inserted_at,
        updated_at: updated_at
      }),
      do: %{
        id: id,
        name: name,
        type: type,
        uri: uri,
        username: username,
        password: password,
        inserted_at: inserted_at,
        updated_at: updated_at
      }
end
