defmodule Aicacia.Autosheets.Web.ApiSpec do
  @behaviour OpenApiSpex.OpenApi

  @impl OpenApiSpex.OpenApi
  def spec,
    do:
      %OpenApiSpex.OpenApi{
        servers: [
          OpenApiSpex.Server.from_endpoint(Aicacia.Autosheets.Web.Endpoint)
        ],
        info: %OpenApiSpex.Info{
          title: Application.spec(:aicacia_autosheets, :description) |> to_string(),
          version: Application.spec(:aicacia_autosheets, :vsn) |> to_string()
        },
        paths: OpenApiSpex.Paths.from_router(Aicacia.Autosheets.Web.Router),
        components: %OpenApiSpex.Components{
          securitySchemes: %{
            "authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}
          }
        }
      }
      |> OpenApiSpex.resolve_schema_modules()
end
