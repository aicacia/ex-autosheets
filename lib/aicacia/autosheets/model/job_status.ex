defmodule Aicacia.Autosheets.Model.JobStatus do
  use EctoEnum.Postgres, type: :job_status, enums: [:running, :success, :failure]
end
