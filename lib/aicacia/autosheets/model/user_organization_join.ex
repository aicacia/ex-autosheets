defmodule Aicacia.Autosheets.Model.UserOrganizationJoin do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "user_organization_joins" do
    belongs_to(:user, Model.User, type: :binary_id)
    belongs_to(:organization, Model.Organization)
  end
end
