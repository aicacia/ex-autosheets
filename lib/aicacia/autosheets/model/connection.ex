defmodule Aicacia.Autosheets.Model.Connection do
  use Ecto.Schema
  import Ecto.Query

  alias Aicacia.Autosheets.Repo
  alias Aicacia.Autosheets.Model

  schema "connections" do
    belongs_to(:organization, Model.Organization)

    field(:name, :string)
    field(:type, Model.ConnectionType)

    timestamps(type: :utc_datetime)
  end

  def get_connection_model(:google_sheet), do: Model.GoogleSheetConnection
  def get_connection_model(:postgres), do: Model.PostgresConnection

  def get_connection_query(%Model.Connection{id: id, type: type}),
    do:
      from(c in get_connection_model(type),
        where: c.connection_id == ^id,
        preload: [:connection],
        select: c
      )

  def get_connection(%Model.Connection{} = connection),
    do:
      get_connection_query(connection)
      |> Repo.one()

  def get_connection!(%Model.Connection{} = connection),
    do:
      get_connection_query(connection)
      |> Repo.one!()
end
