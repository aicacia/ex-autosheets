defmodule Aicacia.Autosheets.Model.GoogleSheetConnection do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "google_sheet_connections" do
    belongs_to(:connection, Model.Connection)

    field(:service_account, :map)

    timestamps(type: :utc_datetime)
  end
end
