defmodule Aicacia.Autosheets.Model.ConnectionType do
  use EctoEnum.Postgres, type: :connection_types, enums: [:google_sheet, :postgres]
end
