defmodule Aicacia.Autosheets.Model.PostgresConnection do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "postgres_connections" do
    belongs_to(:connection, Model.Connection)

    field(:uri, :string)
    field(:username, :string)
    field(:password, :string)

    timestamps(type: :utc_datetime)
  end
end
