defmodule Aicacia.Autosheets.Model.User do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "users" do
    has_many(:owned_organizations, Model.Organization, foreign_key: :owner_id)
    many_to_many(:organizations, Model.Organization, join_through: Model.UserOrganizationJoin)

    field(:username, :string, null: false)
    field(:encrypted_password, :string, null: false)

    timestamps(type: :utc_datetime)
  end
end
