defmodule Aicacia.Autosheets.Model.Organization do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "organizations" do
    belongs_to(:owner, Model.User, type: :binary_id)
    many_to_many(:users, Model.User, join_through: Model.UserOrganizationJoin)

    field(:name, :string, null: false)

    timestamps(type: :utc_datetime)
  end
end
