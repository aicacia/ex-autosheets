defmodule Aicacia.Autosheets.Model.PipeRun do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "pipe_runs" do
    belongs_to(:pipe, Model.Pipe)
    field(:status, Model.JobStatus, default: :running)

    timestamps(type: :utc_datetime)
  end
end
