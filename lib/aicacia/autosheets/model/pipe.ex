defmodule Aicacia.Autosheets.Model.Pipe do
  use Ecto.Schema

  alias Aicacia.Autosheets.Model

  schema "pipes" do
    belongs_to(:organization, Model.Organization)

    field(:name, :string)
    field(:schedule, :string)
    field(:query, :string)

    belongs_to(:source, Model.Connection)
    field(:source_options, :map, default: {})

    belongs_to(:target, Model.Connection)
    field(:target_options, :map, default: {})

    timestamps(type: :utc_datetime)
  end
end
