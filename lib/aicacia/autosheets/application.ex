defmodule Aicacia.Autosheets.Application do
  use Application
  import Supervisor.Spec, warn: false

  def start(_type, _args) do
    children = [
      Aicacia.Autosheets.Repo,
      Aicacia.Autosheets.Web.Telemetry,
      {Phoenix.PubSub, name: Aicacia.Autosheets.PubSub},
      Aicacia.Autosheets.Web.Endpoint,
      Aicacia.Autosheets.Scheduler
    ]

    children =
      if Mix.env() != :test,
        do:
          children ++
            [worker(Task, [&Aicacia.Autosheets.Service.Pipe.Init.init/0], restart: :temporary)],
        else: children

    opts = [strategy: :one_for_one, name: Aicacia.Autosheets.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Aicacia.Autosheets.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
