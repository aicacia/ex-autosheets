defmodule Aicacia.Autosheets.Service.PostgresConnection.Create do
  use Aicacia.Handler

  import Aicacia.Autosheets.Util.Validator, only: [validate_url: 2]

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    belongs_to(:connection, Model.Connection)
    field(:uri, :string)
    field(:username, :string)
    field(:password, :string)
  end

  def changeset(%{} = attrs) do
    %Service.PostgresConnection.Create{}
    |> cast(attrs, [:connection_id, :uri, :username, :password])
    |> foreign_key_constraint(:connection_id)
    |> validate_url(:uri)
    |> validate_required([:connection_id, :uri, :username, :password])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.PostgresConnection{}
      |> cast(command, [:connection_id, :uri, :username, :password])
      |> Repo.insert!()
      |> Repo.preload([:connection])
    end)
  end
end
