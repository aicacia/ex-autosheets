defmodule Aicacia.Autosheets.Service.PostgresConnection.Update do
  use Aicacia.Handler

  import Aicacia.Autosheets.Util.Validator, only: [validate_url: 2]

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
    field(:uri, :string)
    field(:username, :string)
    field(:password, :string)
  end

  def changeset(%{} = attrs) do
    %Service.PostgresConnection.Update{}
    |> cast(attrs, [:id, :uri, :username, :password])
    |> validate_url(:uri)
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      Repo.get!(Model.PostgresConnection, command.id)
      |> change(%{})
      |> cast(command, [:uri, :username, :password])
      |> Repo.update!()
      |> Repo.preload([:connection])
    end)
  end
end
