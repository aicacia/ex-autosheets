defmodule Aicacia.Autosheets.Service.Organization.Update do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
    belongs_to(:owner, Model.User, type: :binary_id)
    field(:name, :string)
  end

  def changeset(%{} = attrs) do
    %Service.Organization.Update{}
    |> cast(attrs, [:id, :owner_id, :name])
    |> validate_required([:id])
    |> unique_constraint(:name)
    |> foreign_key_constraint(:owner_id)
  end

  def from(%Model.Organization{
        id: id,
        owner_id: owner_id,
        name: name
      }) do
    %Service.Pipe.Update{}
    |> cast(
      %{
        id: id,
        owner_id: owner_id,
        name: name
      },
      [
        :id,
        :owner_id,
        :name
      ]
    )
    |> validate_required([:id])
    |> unique_constraint(:name)
    |> foreign_key_constraint(:owner_id)
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.Organization{id: id} =
        Repo.get!(Model.Organization, command.id)
        |> change(%{})
        |> cast(command, [:owner_id, :name])
        |> Repo.update!()

      Service.Organization.Show.get_organization!(id)
    end)
  end
end
