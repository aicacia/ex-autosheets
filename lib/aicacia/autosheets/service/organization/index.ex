defmodule Aicacia.Autosheets.Service.Organization.Index do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    field(:page, :integer)
    field(:size, :integer)
  end

  def changeset(%{} = params) do
    %Service.Organization.Index{}
    |> cast(params, [:page, :size])
    |> validate_required([])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      if Map.has_key?(command, :page) and Map.has_key?(command, :size) do
        from(p in Model.Organization,
          limit: ^command.size,
          offset: ^((command.page - 1) * command.size),
          preload: [:owner]
        )
      else
        from(p in Model.Organization,
          preload: [:owner]
        )
      end
      |> Repo.all()
    end)
  end
end
