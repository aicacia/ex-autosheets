defmodule Aicacia.Autosheets.Service.Organization.Show do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = params) do
    %Service.Organization.Show{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      get_organization!(command.id)
    end)
  end

  def get_organization!(id),
    do:
      from(u in Model.Organization,
        where: u.id == ^id,
        preload: [:owner, :users],
        select: u
      )
      |> Repo.one!()
end
