defmodule Aicacia.Autosheets.Service.Organization.Create do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    belongs_to(:owner, Model.User, type: :binary_id)
    field(:name, :string)
  end

  def changeset(%{} = attrs) do
    %Service.Organization.Create{}
    |> cast(attrs, [:owner_id, :name])
    |> validate_required([:owner_id, :name])
    |> unique_constraint(:name)
    |> foreign_key_constraint(:owner_id)
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.Organization{id: id} =
        %Model.Organization{}
        |> cast(command, [:owner_id, :name])
        |> Repo.insert!()

      Service.Organization.Show.get_organization!(id)
    end)
  end
end
