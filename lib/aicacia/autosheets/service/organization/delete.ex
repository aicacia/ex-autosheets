defmodule Aicacia.Autosheets.Service.Organization.Delete do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = params) do
    %Service.Organization.Delete{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      Repo.get!(Model.Organization, command.id)
      |> Repo.delete!()
    end)
  end
end
