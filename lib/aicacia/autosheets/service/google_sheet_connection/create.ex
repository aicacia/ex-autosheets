defmodule Aicacia.Autosheets.Service.GoogleSheetConnection.Create do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    belongs_to(:connection, Model.Connection)
    field(:service_account, :map)
  end

  def changeset(%{} = attrs) do
    %Service.GoogleSheetConnection.Create{}
    |> cast(service_account_to_json(attrs), [:connection_id, :service_account])
    |> foreign_key_constraint(:connection_id)
    |> validate_required([:connection_id, :service_account])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.GoogleSheetConnection{}
      |> cast(command, [:connection_id, :service_account])
      |> Repo.insert!()
      |> Repo.preload([:connection])
    end)
  end

  def service_account_to_json(%{"service_account" => service_account} = attrs)
      when is_map(service_account),
      do: attrs

  def service_account_to_json(%{service_account: service_account} = attrs)
      when is_map(service_account),
      do: attrs

  def service_account_to_json(%{"service_account" => service_account} = attrs)
      when is_binary(service_account),
      do: Map.put(attrs, "service_account", Jason.decode!(service_account))

  def service_account_to_json(%{service_account: service_account} = attrs)
      when is_binary(service_account),
      do: Map.put(attrs, :service_account, Jason.decode!(service_account))
end
