defmodule Aicacia.Autosheets.Service.GoogleSheetConnection.Update do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
    field(:service_account, :map)
  end

  def changeset(%{} = attrs) do
    %Service.GoogleSheetConnection.Update{}
    |> cast(Service.GoogleSheetConnection.Create.service_account_to_json(attrs), [
      :id,
      :service_account
    ])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      Repo.get!(Model.GoogleSheetConnection, command.id)
      |> change(%{})
      |> cast(command, [:service_account])
      |> Repo.update!()
      |> Repo.preload([:connection])
    end)
  end
end
