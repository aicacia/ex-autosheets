defmodule Aicacia.Autosheets.Service.Pipe.Delete do
  use Aicacia.Handler

  import Aicacia.Autosheets.Service.Pipe.Create, only: [job_name: 1]

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = attrs) do
    %Service.Pipe.Delete{}
    |> cast(attrs, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      pipe = Service.Pipe.Show.get_pipe!(command.id)
      delete_job!(pipe)
      Repo.delete!(pipe)
      pipe
    end)
  end

  def delete_job!(%Model.Pipe{} = pipe) do
    :ok = Aicacia.Autosheets.Scheduler.delete_job(job_name(pipe))
  end
end
