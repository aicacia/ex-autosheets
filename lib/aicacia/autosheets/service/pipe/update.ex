defmodule Aicacia.Autosheets.Service.Pipe.Update do
  use Aicacia.Handler

  import Aicacia.Autosheets.Util.Validator, only: [validate_connection: 4, validate_schedule: 2]

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
    field(:name, :string)
    field(:query, :string)
    field(:schedule, :string)
    field(:source, :string)
    field(:source_id, :integer)
    field(:source_options, :map)
    field(:target, :string)
    field(:target_id, :integer)
    field(:target_options, :map)
  end

  def changeset(%{} = attrs) do
    %Service.Pipe.Update{}
    |> cast(attrs, [
      :id,
      :name,
      :query,
      :schedule,
      :source,
      :source_id,
      :source_options,
      :target,
      :target_id,
      :target_options
    ])
    |> validate_schedule(:schedule)
    |> validate_connection(:source, :source_id, null: true)
    |> validate_connection(:target, :target_id, null: true)
    |> validate_required([:id])
  end

  def from(%Model.Pipe{
        id: id,
        name: name,
        query: query,
        schedule: schedule,
        source_id: source_id,
        source_options: source_options,
        target_id: target_id,
        target_options: target_options
      }) do
    %Service.Pipe.Update{}
    |> cast(
      %{
        id: id,
        name: name,
        query: query,
        schedule: schedule,
        source_id: source_id,
        source_options: source_options,
        target_id: target_id,
        target_options: target_options
      },
      [
        :id,
        :name,
        :query,
        :schedule,
        :source_id,
        :source_options,
        :target_id,
        :target_options
      ]
    )
    |> validate_schedule(:schedule)
    |> validate_connection(:source, :source_id, null: true)
    |> validate_connection(:target, :target_id, null: true)
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      old_pipe = Repo.get!(Model.Pipe, command.id)

      pipe =
        old_pipe
        |> change(%{})
        |> cast(command, [
          :name,
          :query,
          :schedule,
          :source_id,
          :source_options,
          :target_id,
          :target_options
        ])
        |> Repo.update!()

      if old_pipe.name != pipe.name or old_pipe.schedule != pipe.schedule do
        Service.Pipe.Delete.delete_job!(old_pipe)
        Service.Pipe.Create.create_job!(pipe)
      end

      Service.Pipe.Show.get_pipe!(pipe.id)
    end)
  end
end
