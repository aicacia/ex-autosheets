defmodule Aicacia.Autosheets.Service.Pipe.Create do
  use Aicacia.Handler

  import Aicacia.Autosheets.Util.Validator, only: [validate_connection: 3, validate_schedule: 2]

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    belongs_to(:organization, Model.Organization)
    field(:name, :string)
    field(:query, :string)
    field(:schedule, :string)
    field(:source, :string)
    field(:source_id, :integer)
    field(:source_options, :map, default: %{})
    field(:target, :string)
    field(:target_id, :integer)
    field(:target_options, :map, default: %{})
  end

  def changeset(%{} = attrs) do
    %Service.Pipe.Create{}
    |> cast(attrs, [
      :organization_id,
      :name,
      :query,
      :schedule,
      :source,
      :source_id,
      :source_options,
      :target,
      :target_id,
      :target_options
    ])
    |> validate_schedule(:schedule)
    |> validate_connection(:source, :source_id)
    |> validate_connection(:target, :target_id)
    |> validate_required([
      :organization_id,
      :name,
      :query,
      :schedule,
      :source_id,
      :source_options,
      :target_id,
      :target_options
    ])
    |> foreign_key_constraint(:organization_id)
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      pipe =
        %Model.Pipe{}
        |> cast(command, [
          :organization_id,
          :name,
          :query,
          :schedule,
          :source_id,
          :source_options,
          :target_id,
          :target_options
        ])
        |> Repo.insert!()

      create_job!(pipe)

      Service.Pipe.Show.get_pipe!(pipe.id)
    end)
  end

  def job_name(%Model.Pipe{id: id, name: name}),
    do: "#{id}-#{name}" |> Base.encode16() |> String.to_atom()

  def create_job!(%Model.Pipe{id: id, schedule: schedule} = pipe) do
    job =
      Aicacia.Autosheets.Scheduler.new_job()
      |> Quantum.Job.set_name(job_name(pipe))
      |> Quantum.Job.set_schedule(Crontab.CronExpression.Parser.parse!(schedule))
      |> Quantum.Job.set_task({Aicacia.Autosheets.Service.Pipe.Run, :run!, [id]})

    :ok = Aicacia.Autosheets.Scheduler.add_job(job)
    job
  end
end
