defmodule Aicacia.Autosheets.Service.Pipe.Show do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = params) do
    %Service.Pipe.Show{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      get_pipe!(command.id)
    end)
  end

  def get_pipe_query(id),
    do:
      from(p in Model.Pipe,
        where: p.id == ^id,
        preload: [:organization, :source, :target],
        select: p
      )

  def get_pipe!(id) do
    pipe =
      get_pipe_query(id)
      |> Repo.one!()

    pipe
    |> Map.put(:source, Model.Connection.get_connection!(pipe.source))
    |> Map.put(:target, Model.Connection.get_connection!(pipe.target))
  end
end
