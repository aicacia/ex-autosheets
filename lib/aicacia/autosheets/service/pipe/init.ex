defmodule Aicacia.Autosheets.Service.Pipe.Init do
  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  def init() do
    Repo.all(Model.Pipe)
    |> Enum.each(&Service.Pipe.Create.create_job!/1)
  end
end
