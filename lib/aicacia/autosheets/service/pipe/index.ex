defmodule Aicacia.Autosheets.Service.Pipe.Index do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    field(:page, :integer)
    field(:size, :integer)
  end

  def changeset(%{} = params) do
    %Service.Pipe.Index{}
    |> cast(params, [:page, :size])
    |> validate_required([])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      if Map.has_key?(command, :page) and Map.has_key?(command, :size) do
        from(p in Model.Pipe,
          limit: ^command.size,
          offset: ^((command.page - 1) * command.size),
          select: p.id
        )
      else
        from(p in Model.Pipe,
          select: p.id
        )
      end
      |> Repo.all()
      |> Enum.map(fn pipe_id -> Service.Pipe.Show.get_pipe!(pipe_id) end)
    end)
  end
end
