defmodule Aicacia.Autosheets.Service.Pipe.Run do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = attrs) do
    %Service.Pipe.Run{}
    |> cast(attrs, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      pipe = Service.Pipe.Show.get_pipe!(command.id)
      run = %Model.PipeRun{} |> change(%{pipe_id: pipe.id}) |> Repo.insert!()

      try do
        extract!(pipe.query, pipe.source_options, pipe.source)
        |> load!(pipe.target_options, pipe.target)

        run |> change(%{status: :success}) |> Repo.insert!()
      rescue
        _ ->
          run |> change(%{status: :failure}) |> Repo.insert!()
      end

      pipe
    end)
  end

  def extract!(query, %{}, %Model.PostgresConnection{
        uri: uri,
        username: username,
        password: password
      }) do
    %URI{host: hostname, path: path, port: port} = URI.parse(uri)

    {:ok, postgrex_pid} =
      Postgrex.start_link(
        hostname: hostname,
        port: port,
        database: String.replace(path, ~r/\/+/, ""),
        username: username,
        password: password
      )

    %Postgrex.Result{
      columns: columns,
      rows: rows
    } = Postgrex.query!(postgrex_pid, query, [])

    GenServer.stop(postgrex_pid)

    %{
      columns: columns,
      rows: rows
    }
  end

  def load!(
        %{
          columns: columns,
          rows: rows
        },
        %{} = options,
        %Model.GoogleSheetConnection{service_account: service_account}
      ) do
    spreadsheet_id = Map.get(options, "id")

    range = "A1:#{to_alphabet(length(columns))}#{length(rows) + 1}"

    Goth.Config.add_config(service_account)

    {:ok, token} =
      Goth.Token.for_scope(
        {service_account["client_email"], "https://www.googleapis.com/auth/spreadsheets"}
      )

    conn = GoogleApi.Sheets.V4.Connection.new(token.token)

    {:ok, _} =
      GoogleApi.Sheets.V4.Api.Spreadsheets.sheets_spreadsheets_values_update(
        conn,
        spreadsheet_id,
        range,
        valueInputOption: "USER_ENTERED",
        body: %GoogleApi.Sheets.V4.Model.ValueRange{
          majorDimension: "ROWS",
          range: range,
          values: [columns] ++ rows
        }
      )
  end

  def run!(id),
    do:
      Service.Pipe.Run.new!(%{id: id})
      |> Service.Pipe.Run.handle!()

  # TODO: this should wrap for numbers > 26, like 27 -> AA
  defp to_alphabet(integer), do: String.capitalize(List.to_string([96 + rem(integer, 26)]))
end
