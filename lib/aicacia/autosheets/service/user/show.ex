defmodule Aicacia.Autosheets.Service.User.Show do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key {:id, :binary_id, autogenerate: false}
  schema "" do
  end

  def changeset(%{} = params) do
    %Service.User.Show{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      get_user!(command.id)
    end)
  end

  def get_user!(id),
    do:
      from(u in Model.User,
        where: u.id == ^id,
        preload: [:organizations],
        select: u
      )
      |> Repo.one!()
end
