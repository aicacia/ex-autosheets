defmodule Aicacia.Autosheets.Service.User.Update do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
    field(:username, :string)
    field(:password, :string)
    field(:encrypted_password, :string)
  end

  def changeset(%{} = attrs) do
    %Service.User.Update{}
    |> cast(attrs, [:id, :username, :password])
    |> validate_required([:id])
    |> validate_length(:password, min: 8)
    |> unique_constraint(:username)
    |> Service.User.Create.encrypt_password()
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.User{id: id} =
        Repo.get!(Model.User, command.id)
        |> change(%{})
        |> cast(command, [:username, :encrypted_password])
        |> Repo.update!()

      Service.User.Show.get_user!(id)
    end)
  end
end
