defmodule Aicacia.Autosheets.Service.User.Create do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    field(:username, :string)
    field(:password, :string)
    field(:encrypted_password, :string)
  end

  def changeset(%{} = attrs) do
    %Service.User.Create{}
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
    |> validate_length(:password, min: 8)
    |> unique_constraint(:username)
    |> encrypt_password()
    |> validate_required([:encrypted_password])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.User{}
      |> cast(command, [:username, :encrypted_password])
      |> Repo.insert!()
    end)
  end

  def encrypt_password(changeset) do
    case get_field(changeset, :password) do
      nil ->
        changeset

      password ->
        put_change(changeset, :encrypted_password, Bcrypt.hash_pwd_salt(password))
    end
  end
end
