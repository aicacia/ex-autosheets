defmodule Aicacia.Autosheets.Service.User.Verify do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  def invalid_username_password_error, do: "username or password is invalid"

  @primary_key false
  schema "" do
    field(:username, :string)
    field(:password, :string)
    field(:encrypted_password, :string)
    field(:user_id, :binary_id)
  end

  def changeset(%{} = attrs) do
    %Service.User.Verify{}
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
    |> validate_password()
    |> validate_required([:user_id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      Service.User.Show.get_user!(command.user_id)
    end)
  end

  defp validate_password(changeset) do
    case Repo.get_by(Model.User, username: get_field(changeset, :username)) do
      nil ->
        invalid_username_or_password(changeset)

      %Model.User{id: user_id, encrypted_password: encrypted_password} ->
        if Bcrypt.verify_pass(get_field(changeset, :password), encrypted_password) do
          put_change(changeset, :user_id, user_id)
        else
          invalid_username_or_password(changeset)
        end
    end
  end

  defp invalid_username_or_password(changeset) do
    changeset
    |> add_error(:username, invalid_username_password_error())
    |> add_error(:password, invalid_username_password_error())
  end
end
