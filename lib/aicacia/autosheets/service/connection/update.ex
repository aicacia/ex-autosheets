defmodule Aicacia.Autosheets.Service.Connection.Update do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo
  alias Aicacia.Autosheets.Util

  schema "" do
    field(:name, :string)
    field(:params, :map)
  end

  def changeset(%{} = attrs) do
    %Service.Connection.Update{}
    |> cast(attrs, [:id, :name])
    |> put_change(:params, Util.ToAtomMap.to_atom_map(attrs))
    |> validate_required([:id, :params])
  end

  def from(%Model.GoogleSheetConnection{
        connection: %Model.Connection{id: id, name: name, type: type},
        service_account: service_account
      }),
      do: changeset(%{id: id, name: name, type: type, service_account: service_account})

  def from(%Model.PostgresConnection{
        connection: %Model.Connection{id: id, name: name, type: type},
        uri: uri,
        username: username,
        password: password
      }),
      do:
        changeset(%{
          id: id,
          name: name,
          type: type,
          uri: uri,
          username: username,
          password: password
        })

  def handle(%{} = command) do
    Repo.run(fn ->
      Repo.get!(Model.Connection, command.id)
      |> change(%{})
      |> cast(command, [:name])
      |> Repo.update!()
      |> update_connection_type!(command.params)
    end)
  end

  def update_connection_type!(
        %Model.Connection{id: connection_id, type: :google_sheet},
        %{} = attrs
      ) do
    google_sheet_connection =
      Repo.get_by!(Model.GoogleSheetConnection, connection_id: connection_id)

    Service.GoogleSheetConnection.Update.new!(attrs |> Map.put(:id, google_sheet_connection.id))
    |> Service.GoogleSheetConnection.Update.handle!()
  end

  def update_connection_type!(%Model.Connection{id: connection_id, type: :postgres}, %{} = attrs) do
    postgres_connection = Repo.get_by!(Model.PostgresConnection, connection_id: connection_id)

    Service.PostgresConnection.Update.new!(attrs |> Map.put(:id, postgres_connection.id))
    |> Service.PostgresConnection.Update.handle!()
  end
end
