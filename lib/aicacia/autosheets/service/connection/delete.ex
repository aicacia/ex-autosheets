defmodule Aicacia.Autosheets.Service.Connection.Delete do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = params) do
    %Service.Connection.Delete{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      connection = Service.Connection.Show.new!(command) |> Service.Connection.Show.handle!()
      Repo.delete!(connection.connection)
      connection
    end)
  end
end
