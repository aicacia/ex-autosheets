defmodule Aicacia.Autosheets.Service.Connection.Create do
  use Aicacia.Handler

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo
  alias Aicacia.Autosheets.Util

  @primary_key false
  schema "" do
    belongs_to(:organization, Model.Organization)
    field(:name, :string)
    field(:type, Model.ConnectionType, default: :postgres)
    field(:params, :map)
  end

  def changeset(%{} = attrs) do
    %Service.Connection.Create{}
    |> cast(attrs, [:organization_id, :name, :type])
    |> put_change(:params, Util.ToAtomMap.to_atom_map(attrs))
    |> validate_required([:organization_id, :name, :type, :params])
    |> foreign_key_constraint(:organization_id)
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      %Model.Connection{}
      |> cast(command, [:organization_id, :name, :type])
      |> Repo.insert!()
      |> create_connection_type!(command.params)
    end)
  end

  def create_connection_type!(%Model.Connection{id: id, type: :google_sheet}, %{} = attrs),
    do:
      Service.GoogleSheetConnection.Create.new!(attrs |> Map.put(:connection_id, id))
      |> Service.GoogleSheetConnection.Create.handle!()

  def create_connection_type!(%Model.Connection{id: id, type: :postgres}, %{} = attrs),
    do:
      Service.PostgresConnection.Create.new!(attrs |> Map.put(:connection_id, id))
      |> Service.PostgresConnection.Create.handle!()
end
