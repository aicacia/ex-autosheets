defmodule Aicacia.Autosheets.Service.Connection.Show do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  schema "" do
  end

  def changeset(%{} = params) do
    %Service.Connection.Show{}
    |> cast(params, [:id])
    |> validate_required([:id])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      from(c in Model.Connection,
        where: c.id == ^command.id,
        preload: [:organization],
        select: c
      )
      |> Repo.one!()
      |> Model.Connection.get_connection!()
    end)
  end
end
