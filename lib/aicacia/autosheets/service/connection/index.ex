defmodule Aicacia.Autosheets.Service.Connection.Index do
  use Aicacia.Handler
  import Ecto.Query

  alias Aicacia.Autosheets.Model
  alias Aicacia.Autosheets.Service
  alias Aicacia.Autosheets.Repo

  @primary_key false
  schema "" do
    field(:page, :integer)
    field(:size, :integer)
  end

  def changeset(%{} = params) do
    %Service.Connection.Index{}
    |> cast(params, [:page, :size])
    |> validate_required([])
  end

  def handle(%{} = command) do
    Repo.run(fn ->
      all(command)
      |> Enum.map(fn connection ->
        Model.Connection.get_connection!(connection)
      end)
    end)
  end

  def all(%{} = command \\ %{}) do
    if Map.has_key?(command, :page) and Map.has_key?(command, :size) do
      from c in Model.Connection,
        limit: ^command.size,
        offset: ^((command.page - 1) * command.size),
        preload: [:organization],
        select: c
    else
      from c in Model.Connection,
        preload: [:organization],
        select: c
    end
    |> Repo.all()
  end
end
