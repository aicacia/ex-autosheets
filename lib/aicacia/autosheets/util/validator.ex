defmodule Aicacia.Autosheets.Util.Validator do
  import Ecto.Changeset

  alias Aicacia.Autosheets.Repo
  alias Aicacia.Autosheets.Model

  def validate_url(changeset, field, opts \\ []) do
    validate_change(changeset, field, fn _, value ->
      case URI.parse(value) do
        %URI{host: nil} ->
          "missing a host"

        %URI{host: host} ->
          case :inet.gethostbyname(Kernel.to_charlist(host)) do
            {:ok, _} -> nil
            {:error, _} -> "invalid host"
          end
      end
      |> case do
        error when is_binary(error) -> [{field, Keyword.get(opts, :message, error)}]
        _ -> []
      end
    end)
  end

  def validate_connection(changeset, field, field_id_key, opts \\ []) do
    case get_field(changeset, field) do
      nil ->
        case get_field(changeset, field_id_key) do
          nil ->
            nil

          id ->
            Repo.get(Model.Connection, id)
        end

      name ->
        Repo.get_by(Model.Connection, name: name)
    end
    |> case do
      nil ->
        if Keyword.get(opts, :null, false) do
          changeset
        else
          add_error(changeset, field, Keyword.get(opts, :message, "can't be blank"),
            validation: :connection
          )
        end

      %Model.Connection{id: id} = connection ->
        case Model.Connection.get_connection(connection) do
          nil ->
            add_error(changeset, field, Keyword.get(opts, :message, "can't be blank"),
              validation: :connection
            )

          _connection ->
            put_change(changeset, field_id_key, id)
        end
    end
  end

  def validate_schedule(changeset, field, opts \\ []) do
    schedule = get_field(changeset, field)

    if schedule == nil do
      changeset
    else
      case Crontab.CronExpression.Parser.parse(schedule) do
        {:ok, cron_expression} ->
          put_change(changeset, field, Crontab.CronExpression.Composer.compose(cron_expression))

        {:error, message} ->
          add_error(changeset, field, Keyword.get(opts, :message, message), validation: field)
      end
    end
  end
end
