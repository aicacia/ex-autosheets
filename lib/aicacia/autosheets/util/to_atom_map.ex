defmodule Aicacia.Autosheets.Util.ToAtomMap do
  def to_atom_map(map) when is_map(map) do
    for {key, val} <- map,
        into: %{},
        do: {if(is_atom(key), do: key, else: String.to_atom(key)), to_atom_map(val)}
  end

  def to_atom_map(list) when is_list(list) do
    for val <- list, into: [], do: to_atom_map(val)
  end

  def to_atom_map(val),
    do: val
end
