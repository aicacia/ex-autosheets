defmodule Aicacia.Autosheets.Repo do
  use Ecto.Repo,
    otp_app: :aicacia_autosheets,
    adapter: Ecto.Adapters.Postgres

  def run(fun_or_multi, opts \\ []) do
    try do
      Aicacia.Autosheets.Repo.transaction(fun_or_multi, opts)
    rescue
      e -> {:error, e}
    end
  end
end
