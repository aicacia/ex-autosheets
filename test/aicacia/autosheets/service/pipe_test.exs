defmodule Aicacia.Autosheets.Service.PipeTest do
  use Aicacia.Autosheets.Service.Case

  alias Aicacia.Autosheets.Service

  setup do
    user =
      Service.User.Create.new!(%{
        username: "username",
        password: "!!very12long45password!!"
      })
      |> Service.User.Create.handle!()

    organization =
      Service.Organization.Create.new!(%{
        owner_id: user.id,
        name: "Organization"
      })
      |> Service.Organization.Create.handle!()

    postgres_connection =
      Service.Connection.Create.new!(%{
        organization_id: organization.id,
        name: "postgres",
        type: :postgres,
        uri: "postgres://localhost:5432/aicacia_autosheets_test",
        username: "postgres",
        password: "postgres"
      })
      |> Service.Connection.Create.handle!()

    google_sheet_connection =
      Service.Connection.Create.new!(%{
        organization_id: organization.id,
        name: "google_sheet",
        type: :google_sheet,
        service_account: File.read!("test/fixtures/aicacia-59960a60da0c.json") |> Jason.decode!()
      })
      |> Service.Connection.Create.handle!()

    pipe =
      Service.Pipe.Create.new!(%{
        organization_id: organization.id,
        name: "pipe",
        schedule: "0 0 * * * *",
        source: postgres_connection.connection.name,
        query: "SELECT * FROM schema_migrations;",
        target: google_sheet_connection.connection.name,
        target_options: %{
          id: "1gXWT1kvxJGVNDKmJ_eXHfVH3ye4D7Lyk6ZC4_BXVvmA",
          name: "Test"
        }
      })
      |> Service.Pipe.Create.handle!()

    {:ok, pipe: pipe}
  end

  describe "run" do
    test "should write to google sheet", %{pipe: _pipe} do
      # Service.Pipe.Run.run!(pipe.id)
    end
  end
end
