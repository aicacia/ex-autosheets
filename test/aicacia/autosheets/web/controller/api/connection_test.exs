defmodule Aicacia.Autosheets.Web.Controller.Api.ConnectionTest do
  use Aicacia.Autosheets.Web.Case

  alias Aicacia.Autosheets.Service

  setup %{conn: conn} do
    user =
      Service.User.Create.new!(%{
        username: "username",
        password: "!!very12long45password!!"
      })
      |> Service.User.Create.handle!()

    organization =
      Service.Organization.Create.new!(%{
        owner_id: user.id,
        name: "Organization"
      })
      |> Service.Organization.Create.handle!()

    {:ok,
     conn:
       conn
       |> put_req_header("accept", "application/json"),
     user: user,
     organization: organization}
  end

  describe "index" do
    test "should return all connections", %{
      conn: conn,
      organization: organization
    } do
      Service.Connection.Create.new!(%{
        organization_id: organization.id,
        name: "postgres",
        type: :postgres,
        uri: "postgres://localhost:5432/database",
        username: "user",
        password: "pass"
      })
      |> Service.Connection.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_connection_path(@endpoint, :index, organization.id)
        )

      connections_json = json_response(conn, 200)

      assert length(connections_json) == 1
    end

    test "should return an empty list", %{conn: conn, organization: organization} do
      conn =
        get(
          conn,
          Routes.api_organization_connection_path(@endpoint, :index, organization.id)
        )

      connections_json = json_response(conn, 200)

      assert [] == connections_json
    end
  end

  describe "show" do
    test "should return a connection by id", %{
      conn: conn,
      organization: organization
    } do
      connection =
        Service.Connection.Create.new!(%{
          organization_id: organization.id,
          name: "postgres",
          type: :postgres,
          uri: "postgres://localhost:5432/database",
          username: "user",
          password: "pass"
        })
        |> Service.Connection.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_connection_path(
            @endpoint,
            :show,
            organization.id,
            connection.connection.id
          )
        )

      connection_json = json_response(conn, 200)

      assert connection_json["id"] == connection.id
    end

    test "should return 404", %{conn: conn, organization: organization} do
      conn =
        get(
          conn,
          Routes.api_organization_connection_path(@endpoint, :show, organization.id, 1)
        )

      json_response(conn, 404)
    end
  end

  describe "create" do
    test "should return created connection", %{
      conn: conn,
      organization: organization
    } do
      conn =
        post(
          conn,
          Routes.api_organization_connection_path(@endpoint, :create, organization.id),
          %{
            organization_id: organization.id,
            name: "postgres",
            type: :postgres,
            uri: "postgres://localhost:5432/database",
            username: "user",
            password: "pass"
          }
        )

      connection_json = json_response(conn, 201)

      assert connection_json["name"] == "postgres"
    end

    test "should return param errors", %{conn: conn, organization: organization} do
      conn =
        post(
          conn,
          Routes.api_organization_connection_path(@endpoint, :create, organization.id),
          %{
            name: "postgres",
            type: :postgres
          }
        )

      json_response(conn, 422)
    end
  end

  describe "update" do
    test "should return updated connection", %{
      conn: conn,
      organization: organization
    } do
      connection =
        Service.Connection.Create.new!(%{
          organization_id: organization.id,
          name: "postgres",
          type: :postgres,
          uri: "postgres://localhost:5432/database",
          username: "user",
          password: "pass"
        })
        |> Service.Connection.Create.handle!()

      conn =
        put(
          conn,
          Routes.api_organization_connection_path(
            @endpoint,
            :update,
            organization.id,
            connection.connection.id
          ),
          %{
            username: "new_user",
            password: "new_pass"
          }
        )

      connection_json = json_response(conn, 200)

      assert connection_json["name"] == connection.connection.name
      assert connection_json["username"] == "new_user"
      assert connection_json["password"] == "new_pass"
    end

    test "should return param errors", %{conn: conn, organization: organization} do
      connection =
        Service.Connection.Create.new!(%{
          organization_id: organization.id,
          name: "postgres",
          type: :postgres,
          uri: "postgres://localhost:5432/database",
          username: "user",
          password: "pass"
        })
        |> Service.Connection.Create.handle!()

      conn =
        put(
          conn,
          Routes.api_organization_connection_path(
            @endpoint,
            :update,
            organization.id,
            connection.connection.id
          ),
          %{
            uri: "invalid"
          }
        )

      json_response(conn, 422)
    end
  end
end
