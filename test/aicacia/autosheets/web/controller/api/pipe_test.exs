defmodule Aicacia.Autosheets.Web.Controller.Api.PipeTest do
  use Aicacia.Autosheets.Web.Case

  alias Aicacia.Autosheets.Service

  setup %{conn: conn} do
    user =
      Service.User.Create.new!(%{
        username: "username",
        password: "!!very12long45password!!"
      })
      |> Service.User.Create.handle!()

    organization =
      Service.Organization.Create.new!(%{
        owner_id: user.id,
        name: "Organization"
      })
      |> Service.Organization.Create.handle!()

    postgres_connection =
      Service.Connection.Create.new!(%{
        organization_id: organization.id,
        name: "postgres",
        type: :postgres,
        uri: "postgres://localhost:5432/database",
        username: "user",
        password: "pass"
      })
      |> Service.Connection.Create.handle!()

    google_sheet_connection =
      Service.Connection.Create.new!(%{
        organization_id: organization.id,
        name: "google_sheet",
        type: :google_sheet,
        service_account: File.read!("test/fixtures/service_account.json") |> Jason.decode!()
      })
      |> Service.Connection.Create.handle!()

    {:ok,
     conn:
       conn
       |> put_req_header("accept", "application/json"),
     user: user,
     organization: organization,
     postgres_connection: postgres_connection,
     google_sheet_connection: google_sheet_connection}
  end

  describe "index" do
    test "should return all pipes", %{
      conn: conn,
      postgres_connection: postgres_connection,
      google_sheet_connection: google_sheet_connection,
      organization: organization
    } do
      Service.Pipe.Create.new!(%{
        organization_id: organization.id,
        name: "pipe",
        schedule: "0 0 * * * *",
        source_id: postgres_connection.connection.id,
        query: "SELECT * FROM connections;",
        target_id: google_sheet_connection.connection.id
      })
      |> Service.Pipe.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :index, organization.id)
        )

      pipes_json = json_response(conn, 200)

      assert length(pipes_json) == 1
    end

    test "should return an empty list", %{conn: conn, organization: organization} do
      conn =
        get(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :index, organization.id)
        )

      pipes_json = json_response(conn, 200)

      assert [] == pipes_json
    end
  end

  describe "show" do
    test "should return a pipe by id", %{
      conn: conn,
      postgres_connection: postgres_connection,
      google_sheet_connection: google_sheet_connection,
      organization: organization
    } do
      pipe =
        Service.Pipe.Create.new!(%{
          organization_id: organization.id,
          name: "pipe",
          schedule: "0 0 * * * *",
          source_id: postgres_connection.connection.id,
          query: "SELECT * FROM connections;",
          target_id: google_sheet_connection.connection.id
        })
        |> Service.Pipe.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :show, organization.id, pipe.id)
        )

      pipe_json = json_response(conn, 200)

      assert pipe_json["id"] == pipe.id
    end

    test "should return 404", %{conn: conn, organization: organization} do
      conn =
        get(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :show, organization.id, 1)
        )

      json_response(conn, 404)
    end
  end

  describe "create" do
    test "should return created pipe", %{
      conn: conn,
      postgres_connection: postgres_connection,
      google_sheet_connection: google_sheet_connection,
      organization: organization
    } do
      conn =
        post(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :create, organization.id),
          %{
            organization_id: organization.id,
            name: "pipe",
            schedule: "0 0 * * * *",
            source_id: postgres_connection.connection.id,
            query: "SELECT * FROM connections;",
            target_id: google_sheet_connection.connection.id
          }
        )

      pipe_json = json_response(conn, 201)

      assert pipe_json["name"] == "pipe"
    end

    test "should return param errors", %{conn: conn, organization: organization} do
      conn =
        post(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :create, organization.id),
          %{
            organization_id: organization.id,
            name: "pipe"
          }
        )

      json_response(conn, 422)
    end
  end

  describe "update" do
    test "should return updated pipe", %{
      conn: conn,
      postgres_connection: postgres_connection,
      google_sheet_connection: google_sheet_connection,
      organization: organization
    } do
      pipe =
        Service.Pipe.Create.new!(%{
          organization_id: organization.id,
          name: "pipe",
          schedule: "0 0 * * * *",
          source_id: postgres_connection.connection.id,
          query: "SELECT * FROM connections;",
          target_id: google_sheet_connection.connection.id
        })
        |> Service.Pipe.Create.handle!()

      conn =
        put(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :update, organization.id, pipe.id),
          %{
            schedule: "0 * * * * *",
            query: "SELECT * FROM pipes;"
          }
        )

      pipe_json = json_response(conn, 200)

      assert pipe_json["name"] == pipe.name
      assert pipe_json["schedule"] == "0 * * * * *"
      assert pipe_json["query"] == "SELECT * FROM pipes;"
    end

    test "should return param errors", %{
      conn: conn,
      postgres_connection: postgres_connection,
      google_sheet_connection: google_sheet_connection,
      organization: organization
    } do
      pipe =
        Service.Pipe.Create.new!(%{
          organization_id: organization.id,
          name: "pipe",
          schedule: "0 0 * * * *",
          source_id: postgres_connection.connection.id,
          query: "SELECT * FROM connections;",
          target_id: google_sheet_connection.connection.id
        })
        |> Service.Pipe.Create.handle!()

      conn =
        put(
          conn,
          Routes.api_organization_pipe_path(@endpoint, :update, organization.id, pipe.id),
          %{
            schedule: "invalid"
          }
        )

      json_response(conn, 422)
    end
  end
end
