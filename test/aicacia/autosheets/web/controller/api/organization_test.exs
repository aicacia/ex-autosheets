defmodule Aicacia.Autosheets.Web.Controller.Api.OrganizationTest do
  use Aicacia.Autosheets.Web.Case

  alias Aicacia.Autosheets.Service

  setup %{conn: conn} do
    user =
      Service.User.Create.new!(%{
        username: "username",
        password: "!!very12long45password!!"
      })
      |> Service.User.Create.handle!()

    {:ok,
     conn:
       conn
       |> put_req_header("accept", "application/json"),
     user: user}
  end

  describe "index" do
    test "should return all organizations", %{
      conn: conn,
      user: user
    } do
      Service.Organization.Create.new!(%{
        owner_id: user.id,
        name: "organization"
      })
      |> Service.Organization.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_path(@endpoint, :index)
        )

      organizations_json = json_response(conn, 200)

      assert length(organizations_json) == 1
    end

    test "should return an empty list", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.api_organization_path(@endpoint, :index)
        )

      organizations_json = json_response(conn, 200)

      assert [] == organizations_json
    end
  end

  describe "show" do
    test "should return a organization by id", %{
      conn: conn,
      user: user
    } do
      organization =
        Service.Organization.Create.new!(%{
          owner_id: user.id,
          name: "organization"
        })
        |> Service.Organization.Create.handle!()

      conn =
        get(
          conn,
          Routes.api_organization_path(@endpoint, :show, organization.id)
        )

      organization_json = json_response(conn, 200)

      assert organization_json["id"] == organization.id
    end

    test "should return 404", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.api_organization_path(@endpoint, :show, 1)
        )

      json_response(conn, 404)
    end
  end

  describe "create" do
    test "should return created organization", %{
      conn: conn,
      user: user
    } do
      conn =
        post(
          conn,
          Routes.api_organization_path(@endpoint, :create),
          %{
            owner_id: user.id,
            name: "organization"
          }
        )

      organization_json = json_response(conn, 201)

      assert organization_json["name"] == "organization"
    end

    test "should return param errors", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.api_organization_path(@endpoint, :create),
          %{
            name: "organization"
          }
        )

      json_response(conn, 422)
    end
  end

  describe "update" do
    test "should return updated organization", %{
      conn: conn,
      user: user
    } do
      organization =
        Service.Organization.Create.new!(%{
          owner_id: user.id,
          name: "organization"
        })
        |> Service.Organization.Create.handle!()

      conn =
        put(
          conn,
          Routes.api_organization_path(@endpoint, :update, organization.id),
          %{
            name: "new_organization"
          }
        )

      organization_json = json_response(conn, 200)

      assert organization_json["name"] == "new_organization"
    end
  end
end
