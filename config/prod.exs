use Mix.Config

config :aicacia_autosheets, Aicacia.Autosheets.Web.Endpoint,
  http: [:inet6, port: 4000],
  url: [host: "localhost", port: 4000],
  cache_static_manifest: "priv/static/cache_manifest.json"

config :logger, level: :info

config :peerage,
  dns_name: "autosheets-aicacia-autosheets.api"

config :aicacia_autosheets, Aicacia.Autosheets.Repo,
  show_sensitive_data_on_connection_error: false,
  pool_size: 30
