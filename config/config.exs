use Mix.Config

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    (Mix.env() == "prod" &&
       raise """
       environment variable SECRET_KEY_BASE is missing.
       You can generate one by calling: mix phx.gen.secret
       """)

database_host =
  System.get_env("DATABASE_HOST") ||
    (Mix.env() == "prod" &&
       raise """
       environment variable DATABASE_HOST is missing.
       """)

guardian_token =
  System.get_env("GUARDIAN_TOKEN") ||
    (Mix.env() == "prod" &&
       raise """
       environment variable GUARDIAN_TOKEN is missing.
       You can generate one by calling: mix guardian.gen.secret
       """)

stripe_secret =
  System.get_env("STRIPE_SECRET") ||
    (Mix.env() == "prod" &&
       raise """
       environment variable STRIPE_SECRET is missing.
       """)

config :aicacia_autosheets,
  generators: [binary_id: true],
  ecto_repos: [Aicacia.Autosheets.Repo]

config :aicacia_autosheets, Aicacia.Autosheets.Web.Endpoint,
  url: [host: "localhost"],
  check_origin: false,
  secret_key_base: secret_key_base,
  render_errors: [view: Aicacia.Autosheets.Web.View.Error, accepts: ~w(html json), layout: false],
  pubsub_server: Aicacia.Autosheets.PubSub,
  live_view: [signing_salt: "ozGtcOcL"]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

config :aicacia_autosheets, Aicacia.Autosheets.Scheduler, debug_logging: true

config :stripity_stripe, api_key: stripe_secret

config :goth,
  disabled: true

config :peerage,
  via: Peerage.Via.Dns,
  log_results: false,
  dns_name: "localhost",
  app_name: "aicacia_autosheets"

config :cors_plug,
  origin: ~r/.*/,
  methods: ["GET", "HEAD", "POST", "PUT", "PATCH", "DELETE"]

config :aicacia_autosheets, Aicacia.Autosheets.Web.Guardian,
  issuer: "aicacia_autosheets",
  secret_key: guardian_token

config :aicacia_autosheets, Aicacia.Autosheets.Repo,
  username: "postgres",
  password: "postgres",
  database: "aicacia_autosheets_#{Mix.env()}",
  hostname: database_host,
  show_sensitive_data_on_connection_error: true

import_config "#{Mix.env()}.exs"
