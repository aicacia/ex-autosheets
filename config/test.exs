use Mix.Config

config :aicacia_autosheets, Aicacia.Autosheets.Web.Endpoint,
  http: [port: 4002],
  server: false

config :aicacia_autosheets, Aicacia.Autosheets.Repo, pool: Ecto.Adapters.SQL.Sandbox

config :logger, level: :warn
